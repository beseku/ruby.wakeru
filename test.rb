require 'classifier'
require 'csv'

to_train = CSV.read("data/train.csv", { 
  encoding: "UTF-8", 
  headers: true, 
  header_converters: :symbol,
  converters: :all 
}).map { |d| d.to_hash }

to_learn = CSV.read("data/learn.csv", { 
  encoding: "UTF-8", 
  headers: true, 
  header_converters: :symbol,
  converters: :all 
}).map { |d| d.to_hash }

categories = to_train.map{ |d| d[:answer] }.uniq

engine = Classifier::Bayes.new *categories
output = []

to_train.each { |d| engine.train d[:answer], d[:question] }
to_learn.each { |d| output << [d[:question]] + engine.classifications(d[:question]).values }

CSV.open("data/score.csv", "w") do |csv|
  csv << ['Question'] + categories
  output.each { |o| csv << o }
end